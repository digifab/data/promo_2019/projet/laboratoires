<?php 
class table
{
    function table_actualite()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM actualite");

        while ($donnees = $req->fetch())
        {
        	echo 
        	 "id du post : ".$donnees['id_post']
        	."titre du post : ".$donnees['title_post']
        	."texte du post : ".$donnees['text_post'];
        }
    }

    function table_espace_membres()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM espace_membres");
			echo "<div style='overflow-x:auto;'><table>
					<thead>
						<tr>
							<th><p>id du Membre </p></th>
							<th><p>pseudo du membre</p></th>
							<th><p>password du membre</p></th>
							<th><p>email du membre</p></th>
							<th><p>Date dinscription du membre</p></th>
						</tr>
					</thead>
					<tbody>
					";
        while ($donnees = $req->fetch())
        {
        	echo "<tr><td><p>".$donnees['id_membre']."</p></td>
        		<td><p>".$donnees['pseudo_membre']."</p></td>
        		<td><p>".$donnees['password_membre']."</p></td>
        		<td><p>".$donnees['email_membre']."</p></td>
				<td><p>".$donnees['date_membre']."</p></td></tr>";
        }
		echo "</tbody></table></div>";
    }

    function table_forum_categorie()
    {
    	include("init.php");

    	$req = $sql->query("SELECT * FROM forum_categorie");

        while ($donnees = $req->fetch())
        {
        	echo 
        	 "<br /> id categorie : ".$donnees['id_categorie']
        	."<br /> titre de la categorie : ".$donnees['titre_categorie']
        	."<br /> Heure de la categorie : ".$donnees['heure_categorie'];
        }
    }

    function table_forum_post()
    {
    	include("init.php");

    	$req = $sql->query("SELECT * FROM forum_post");

        while ($donnees = $req->fetch())
        {
        	echo 
        	 "<br /> id post : ".$donnees['id_post']
        	."<br /> titre du post : ".$donnees['titre_post']
        	."<br /> texte du post : ".$donnees['texte_post']
        	."<br /> Heure du post : ".$donnees['heure_post'];
        }
    }

    function table_forum_section()
    {
    	include("init.php");

    	$req = $sql->query("SELECT * FROM forum_section");
		echo "<table>
				<thead>
					<tr>
						<th colspan='2'>haut de table</th>
					</tr>
				</thead>
				<tbody>
			 ";
        while ($donnees = $req->fetch())
        {
        	echo 
        	 "<tr><td>id post : ".$donnees['id_section']."</td>
			 <td>titre du post : ".$donnees['titre_section']."</td>
			 <td>texte du post : ".$donnees['description_section']."</td>
			 <td>Heure du post : ".$donnees['heure_section']."</td></tr>";
        }
		echo"</tbody></table>";
	}

    function table_gen_action()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_action");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>id action </p></th>
                            <th><p>nom action</p></th>
                            <th><p>Univers action</p></th>
                            <th><p>Cible action</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_action']."</p></td>
                <td><p>".$donnees['nom_action']."</p></td>
                <td><p>".$donnees['univers_action']."</p></td>
                <td><p>".$donnees['cible_action']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

    function table_gen_destination()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_destination");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>id destination </p></th>
                            <th><p>nom de la destination</p></th>
                            <th><p>Univers de la destination</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_destination']."</p></td>
                <td><p>".$donnees['nom_destination']."</p></td>
                <td><p>".$donnees['univers_destination']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

    function table_gen_lieux()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_lieux");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>Id lieux</p></th>
                            <th><p>Nom lieux</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_lieux']."</p></td>
                <td><p>".$donnees['nom_lieux']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

    function table_gen_motivation()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_motivation");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>id motivation </p></th>
                            <th><p>nom motivation</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_motivation']."</p></td>
                <td><p>".$donnees['nom_motivation']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

    function table_gen_objectif()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_objectif");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>id objectif </p></th>
                            <th><p>type objectif</p></th>
                            <th><p>Univers objectif</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_objectif']."</p></td>
                <td><p>".$donnees['type_objectif']."</p></td>
                <td><p>".$donnees['univers_objectif']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

    function table_gen_perso_imp()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_perso_imp");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>id personnage</p></th>
                            <th><p>fct personnage</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_perso_imp']."</p></td>
                <td><p>".$donnees['fct_perso_imp']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

    function table_gen_univers()
    {
        include("init.php");

        $req = $sql->query("SELECT * FROM gen_univers");
            echo "<div style='overflow-x:auto;'><table>
                    <thead>
                        <tr>
                            <th><p>id Univers</p></th>
                            <th><p>nom Univers</p></th>
                            <th><p>Type Univers</p></th>
                        </tr>
                    </thead>
                    <tbody>
                    ";
        while ($donnees = $req->fetch())
        {
            echo "<tr><td><p>".$donnees['ID_univers']."</p></td>
                <td><p>".$donnees['nom_univers']."</p></td>
                <td><p>".$donnees['type_univers']."</p></td></tr>";
        }
        echo "</tbody></table></div>";
    }

}
/************************************   FONCTION PRIMAIRE    ***************************************/















/************************************   INJONCTION   ***********************************************/

function espace_membres()
{
	$table_bdd = new table;
	$table_bdd->table_espace_membres();
}

function actualite()
{
	$table_bdd = new table;
	$table_bdd->table_actualite();
}

function forum_categorie()
{
	$table_bdd = new table;
	$table_bdd->table_forum_categorie();
}

function forum_post()
{
	$table_bdd = new table;
	$table_bdd->table_forum_post();
}

function forum_section()
{
	$table_bdd = new table;
	$table_bdd->table_forum_section();
}

function gen_action()
{
    $table_bdd = new table;
    $table_bdd->table_gen_action();
}

function gen_destination()
{
    $table_bdd = new table;
    $table_bdd->table_gen_destination();
}

function gen_lieux()
{
    $table_bdd = new table;
    $table_bdd->table_gen_lieux();
}

function gen_motivation()
{
    $table_bdd = new table;
    $table_bdd->table_gen_motivation();
}

function gen_objectif()
{
    $table_bdd = new table;
    $table_bdd->table_gen_objectif();
}

function gen_perso_imp()
{
    $table_bdd = new table;
    $table_bdd->table_gen_perso_imp();
}

function gen_univers()
{
    $table_bdd = new table;
    $table_bdd->table_gen_univers();
}


















?>